import axios from 'axios'

export default class Api {
  constructor () {
    this.serverUrl = 'http://localhost:8010/';
    this.getWidgetsUrl = this.serverUrl + 'widgets';
  }

  getWidgets () {
    return axios.get(this.getWidgetsUrl);
  }
}
